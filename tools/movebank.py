import os
import subprocess
import pandas as pd


def download_movebank(study_id='000000', username='ericstofferahn', password='CSPdacqre2019!', public=True):
    if public:
        print(['curl', '-v', '-u', f'{username}:{password}', '-o', 'movebank.csv', f'https://www.movebank.org/movebank/service/direct-read?entity_type=event&study_id={study_id}'])
        subprocess.run(['curl', '-v', '-u', f'{username}:{password}', '-o', 'movebank.csv', f'https://www.movebank.org/movebank/service/direct-read?entity_type=event&study_id={study_id}'])
    else:
        subprocess.run(['curl', '-v', '-u', f'{username}:{password}', '-c', 'cookies.txt', '-o', 'license_terms.txt' f'https://www.movebank.org/movebank/service/direct-read?entity_type=event&study_id={study_id}'])
        md5_command = subprocess.Popen(['md5sum', 'license_terms.txt'], stdout=subprocess.PIPE, shell=False)
        my_md5  = md5_command.communicate()
        del md5_command
        subprocess.run(['curl', '-v', '-u', f'{username}:{password}', '-b', 'cookies.txt', '-o', 'movebank.csv', f'https://www.movebank.org/movebank/service/direct-read?entity_type=event&study_id={study_id}&license-md5={my_md5}'])
        os.remove('license_terms.txt')
        os.remove('cookies.txt')

def get_movebank(**mb_params):
    if not os.path.isfile('movebank.csv'):
        print(mb_params)
        download_movebank(**mb_params)
    df_all = pd.read_csv('movebank.csv')
    df = df_all[['location_lat', 'location_long']]
    df.drop_duplicates(keep='first', inplace=True)
    df.dropna(how='any', inplace=True)
    df['PlotKey'] = [f'movement_location_{str(x).zfill(6)}' for x in range(len(df['location_lat']))]
    df.rename(columns={'location_lat': "Latitude", 'location_long': "Longitude"}, inplace=True)
    df = df[['PlotKey', 'Latitude', 'Longitude']]
    df.to_csv(os.path.join('collections', f'movebank{mb_params["study_id"]}_points_v1.csv'), index=False)
    os.remove('movebank.csv')
