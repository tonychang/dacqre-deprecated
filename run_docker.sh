#!/bin/bash
dockerhub_image="ericstofferahn/dacqre:public"

docker run -it --rm -v $(pwd):/contents -v $(pwd)/authentication/credentials:/root/.config/earthengine/credentials -v $(pwd)/authentication/.boto:/root/.boto -w /contents $dockerhub_image /bin/bash
