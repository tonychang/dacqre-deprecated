#!/bin/bash
dockerhub_image="ericstofferahn/dacqre:public"

docker run -it --rm -v $(pwd):/contents -w /contents $dockerhub_image python run-vis.py
